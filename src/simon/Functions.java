package simon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
//import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import java.util.Timer;

public class Functions {

    static int time = 1000;
    public int x =1;
    public Timer t = new Timer();

    //----------------------------------------------------------- פונקציית צעד הבא//
    public void stopTimers(GameFrame game) {
        if (game.timerBlue.isRunning() || game.timerGreen.isRunning() || game.timerRed.isRunning() || game.timerYellow.isRunning()) {
            game.timerGreen.stop();
            game.timerBlue.stop();
            game.timerRed.stop();
            game.timerYellow.stop();
            game.lblRed_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Red_Light.jpg")));
            game.lblBlue_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Blue_Light.jpg")));
            game.lblGreen_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Green_Light.jpg")));
            game.lblYellow_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Yellow_Light.jpg")));
            time += 1000;
        }
    }

    public synchronized void timer(ImageIcon icon, JLabel lbl) {

        t.schedule(new TimerTask() {
            @Override
            public void run() {
                lbl.setIcon(icon);
                x=1;
                //t.cancel();
                
            }
        }, 2000);
    }

    /**
     * this function adds a random number to the array and
     *
     * @param game - the main game form to check
     */
    public void nextMove(GameFrame game) {
        time = 1000;
        game.lblLevel.setText("Level : " + game.Level);
        Random rand = new Random();
        game.Random_num = 1 + rand.nextInt(4);
        game.movesArray.add(game.Random_num);
        stopTimers(game);

        for (int i = 0; i < game.movesArray.size(); i++) {
            
            if(game.movesArray.get(i) == 1){
                stopTimers(game);
                game.lblGreen_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Green.jpg")));
                timer(new ImageIcon(getClass().getResource("/Images/images_Green_Light.jpg")), game.lblGreen_btn);
                //game.timerGreen.start();
            }
            if(game.movesArray.get(i) == 2){
                stopTimers(game);
                game.lblRed_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Red.jpg")));
                timer(new ImageIcon(getClass().getResource("/Images/images_Red_Light.jpg")), game.lblRed_btn);
                //game.timerRed.start();
            }
            if(game.movesArray.get(i) == 3){
                stopTimers(game);
                game.lblYellow_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Yellow.jpg")));
                timer(new ImageIcon(getClass().getResource("/Images/images_Yellow_Light.jpg")), game.lblYellow_btn);
                //game.timerYellow.start();
            }
            if(game.movesArray.get(i) == 4){
                stopTimers(game);
                game.lblBlue_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Blue.jpg")));
                timer(new ImageIcon(getClass().getResource("/Images/images_Blue_Light.jpg")), game.lblBlue_btn);
                //game.timerBlue.start();
            }
            
                /*
                switch (game.movesArray.get(i)) {
                case 1: {
                System.out.println("1");
                stopTimers(game);
                while (x!=1) {
                System.out.println(" -> " + x);
                }
                x=2;
                game.lblGreen_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Green.jpg")));
                timer(new ImageIcon(getClass().getResource("/Images/images_Green_Light.jpg")), game.lblGreen_btn);
                
                //game.timerGreen.start();
                break;
                }
                case 2: {
                System.out.println("2");
                stopTimers(game);
                while (x!=1) {
                System.out.println(" -> " + x);
                }
                x=2;
                game.lblRed_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Red.jpg")));
                timer(new ImageIcon(getClass().getResource("/Images/images_Red_Light.jpg")), game.lblRed_btn);
                
                //game.timerRed.start();
                break;
                }
                case 3: {
                System.out.println("3");
                stopTimers(game);
                while (x!=1) {
                System.out.println(" -> " + x);
                }
                x=2;
                game.lblYellow_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Yellow.jpg")));
                
                timer(new ImageIcon(getClass().getResource("/Images/images_Yellow_Light.jpg")), game.lblYellow_btn);
                //game.timerYellow.start();
                break;
                }
                case 4: {
                System.out.println("4");
                stopTimers(game);
                while (x!=1) {
                System.out.println(" -> " + x);
                }
                x=2;
                game.lblBlue_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Blue.jpg")));
                
                timer(new ImageIcon(getClass().getResource("/Images/images_Blue_Light.jpg")), game.lblBlue_btn);
                //game.timerBlue.start();
                break;
                }
                } // switch
                */
            
        } // for
        System.out.println("The Random number is: " + game.Random_num);
        System.out.println("The Array: " + game.movesArray);
        System.out.println("Level is : " + game.Level++ + "\n");
        game.lblStatus.setText("Status");
    }
}
