package simon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Login extends javax.swing.JFrame {


      public Login() {
        initComponents();
        pack();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnLogin = new javax.swing.JButton();
        txtNickName = new javax.swing.JTextField();
        lblNickName = new javax.swing.JLabel();
        lblPassword = new javax.swing.JLabel();
        lblLoginStatus = new javax.swing.JLabel();
        psfPassword = new javax.swing.JPasswordField();
        cbxShow_Password = new javax.swing.JCheckBox();
        btnNewAccount = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(445, 482));

        btnLogin.setText("Login");
        btnLogin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnLoginMouseClicked(evt);
            }
        });

        lblNickName.setText("Nick Name");

        lblPassword.setText("Password");

        cbxShow_Password.setText("Show Password");
        cbxShow_Password.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxShow_PasswordActionPerformed(evt);
            }
        });

        btnNewAccount.setText("Dont have account ? click here");
        btnNewAccount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnNewAccountMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNewAccount, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblLoginStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLogin))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNickName)
                            .addComponent(lblPassword))
                        .addGap(111, 111, 111)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbxShow_Password)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtNickName, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                                .addComponent(psfPassword)))
                        .addGap(0, 78, Short.MAX_VALUE)))
                .addGap(35, 35, 35))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNickName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNickName))
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPassword)
                    .addComponent(psfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(cbxShow_Password)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 121, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnLogin, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(lblLoginStatus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnNewAccount))
                .addGap(34, 34, 34))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoginMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnLoginMouseClicked
        
        String NickName= txtNickName.getText();
        String Password = psfPassword.getText();
        
        try
        {
            String url = "jdbc:sqlserver://localhost:1433;databaseName=Simon_Project;integratedSecurity=true";   
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn;
              conn = DriverManager.getConnection(url);
            System.out.println("connection created");
            Statement st;
            st = conn.createStatement();
            String sql="Exec Login_User" + "'" + NickName + "'" + "," 
                    + "'" + Password + "'";
            ResultSet rs=st.executeQuery(sql);
            if(rs.next())
            {
                System.out.println(rs.getString(1));
                if("0".equals(rs.getString(1)))
                {
                    lblLoginStatus.setText("Wrong Insert");
                }
                else
                {
                    this.dispose();
                    GameFrame frmG = new GameFrame();
                    frmG.pack();
                    frmG.setLocationRelativeTo(null);
                    frmG.setVisible(true);
                    
                }
            }
            
        }
        
        /* Catch Sql exception */
        catch(SQLException sqle)
        {
            System.out.println("Sql exception "+sqle);
        }
        
        /* Catch Class NotFound Exception */
        catch (ClassNotFoundException notfe)
        {
             System.out.println("Class Not Found Exception "+notfe);
        }       
        finally
        {
            
        }  
        
    }//GEN-LAST:event_btnLoginMouseClicked

    private void cbxShow_PasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxShow_PasswordActionPerformed
        // TODO add your handling code here:
        if(cbxShow_Password.isSelected())
        {
            psfPassword.setEchoChar((char)0);
        }
        else
        {
            psfPassword.setEchoChar('*');
        }
    }//GEN-LAST:event_cbxShow_PasswordActionPerformed

    private void btnNewAccountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewAccountMouseClicked
        
        new Register().setVisible(true);
        this.dispose();
        
    }//GEN-LAST:event_btnNewAccountMouseClicked

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel btnNewAccount;
    private javax.swing.JCheckBox cbxShow_Password;
    private javax.swing.JLabel lblLoginStatus;
    private javax.swing.JLabel lblNickName;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JPasswordField psfPassword;
    private javax.swing.JTextField txtNickName;
    // End of variables declaration//GEN-END:variables
}
