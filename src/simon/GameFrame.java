package simon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class GameFrame extends javax.swing.JFrame {

public GameFrame() {
        initComponents();
        setLocationRelativeTo(null);
    }
    Functions functions = new Functions();
    ArrayList<Integer> movesArray = new ArrayList<>();
    int Random_num, j = 0;
    int click = 0 ;
    int Level=1;
    

//------------------------------------------------------------------------------
  
//----------------------------------------------------------פונקצית בדיקת צעד //
 public void checkMove(int i)
 {
    for (; j < movesArray.size();) {
        System.out.println("j: " +j);
        System.out.println("i: " +i);
        System.out.println("array size: " + movesArray.size());
        if(movesArray.get(j) == i) {
            if (movesArray.get(j/*++*/) == movesArray.get(movesArray.size() - 1))
                lblStatus.setText("You won this round");
            else {
                lblStatus.setText("correct move");
                //j++;
            }
            //j++;
        }
        else {
            JOptionPane.showMessageDialog(null, "You lost");
            dispose();
            new GameFrame().setVisible(true);
        }
        j++;
    }
 }
//------------------------------------------------------------------------------ 

@SuppressWarnings("unchecked") 
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblRed_btn = new javax.swing.JLabel();
        lblGreen_btn = new javax.swing.JLabel();
        lblBlue_btn = new javax.swing.JLabel();
        lblYellow_btn = new javax.swing.JLabel();
        btnExit = new javax.swing.JButton();
        btnRestartGame = new javax.swing.JButton();
        btnNextTurn = new javax.swing.JButton();
        lblPlayerName = new javax.swing.JLabel();
        lblLevel = new javax.swing.JLabel();
        lblSaves = new javax.swing.JLabel();
        btnDraft = new javax.swing.JButton();
        lblStatus = new javax.swing.JLabel();

        jLabel6.setText("jLabel6");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setPreferredSize(new java.awt.Dimension(649, 531));

        lblRed_btn.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        lblRed_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/images_Red_Light.jpg"))); // NOI18N
        lblRed_btn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblRed_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblRed_btnMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblRed_btnMouseReleased(evt);
            }
        });

        lblGreen_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/images_Green_Light.jpg"))); // NOI18N
        lblGreen_btn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblGreen_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblGreen_btnMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblGreen_btnMouseReleased(evt);
            }
        });

        lblBlue_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/images_Blue_Light.jpg"))); // NOI18N
        lblBlue_btn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblBlue_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblBlue_btnMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblBlue_btnMouseReleased(evt);
            }
        });

        lblYellow_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/images_Yellow_Light.jpg"))); // NOI18N
        lblYellow_btn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblYellow_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblYellow_btnMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblYellow_btnMouseReleased(evt);
            }
        });

        btnExit.setText("Exit");
        btnExit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnExitMouseClicked(evt);
            }
        });

        btnRestartGame.setText("Restart Game");
        btnRestartGame.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnNextTurn.setText("Start");
        btnNextTurn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnNextTurnMouseClicked(evt);
            }
        });

        lblPlayerName.setText("Player Name :");

        lblLevel.setText("Level :");

        lblSaves.setText("Saves : ");

        btnDraft.setText("Draft");
        btnDraft.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDraft.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnDraftMouseClicked(evt);
            }
        });

        lblStatus.setText("Status");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblPlayerName, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblLevel, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnNextTurn, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(58, 58, 58)
                        .addComponent(lblStatus)
                        .addGap(115, 115, 115))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(lblGreen_btn)
                            .addGap(43, 43, 43)
                            .addComponent(lblRed_btn))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(lblYellow_btn)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblBlue_btn))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(95, 95, 95)
                        .addComponent(btnDraft)
                        .addContainerGap(104, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnRestartGame, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnExit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(43, 43, 43))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(lblSaves, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(lblPlayerName)
                        .addGap(18, 18, 18)
                        .addComponent(lblLevel)
                        .addGap(18, 18, 18)
                        .addComponent(lblSaves)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnDraft)
                        .addGap(19, 19, 19))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblRed_btn, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblGreen_btn, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(263, 263, 263)
                        .addComponent(btnRestartGame, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnExit, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnNextTurn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblStatus))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblBlue_btn)
                            .addComponent(lblYellow_btn))))
                .addGap(49, 49, 49))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 847, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 627, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel1.getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

//---------------------------------------------------------------------------------------טיימרים//
  public Timer timerGreen  = new Timer(functions.time, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            lblGreen_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Green_Light.jpg")));
            timerGreen.stop();
        }
    });
    
  public Timer timerRed    = new Timer(functions.time, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            lblRed_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Red_Light.jpg")));
            timerRed.stop();
        }
    });
    
  public Timer timerYellow = new Timer(functions.time, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            lblYellow_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Yellow_Light.jpg")));
            timerYellow.stop();
        }
    });
    
  public Timer timerBlue   = new Timer(functions.time, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent ae) {
            lblBlue_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Blue_Light.jpg")));
            
            timerBlue.stop();
        }
    });
//------------------------------------------------------------------------------------------------
  
/////////////////////////******  Events  ******/////////////////////////////////
  
    private void btnDraftMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDraftMouseClicked
        /*timerGreen.start();
        lblBlue_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Blue.jpg")));
        */
        functions.nextMove(this);
        
    }//GEN-LAST:event_btnDraftMouseClicked

    private void btnNextTurnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNextTurnMouseClicked
        btnNextTurn.setText("Next Turn");
        
        functions.nextMove(this);
        //nextMove();
        /*if (click++%2==0)
        {
            nextMove();
        }
          */     
    }//GEN-LAST:event_btnNextTurnMouseClicked


//-------------------------------------לחצן צהוב-----------------------------------------------
    
    private void lblYellow_btnMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblYellow_btnMousePressed

        lblYellow_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Yellow.jpg")));
        if (!lblStatus.equals("You won this round"))
            checkMove(3);
        else j = 0;
        
    }//GEN-LAST:event_lblYellow_btnMousePressed

    private void lblYellow_btnMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblYellow_btnMouseReleased
        
         lblYellow_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Yellow_Light.jpg")));
         
    }//GEN-LAST:event_lblYellow_btnMouseReleased

//-------------------------------------לחצן ירוק-----------------------------------------------
    
    private void lblGreen_btnMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGreen_btnMousePressed
  
        lblGreen_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Green.jpg")));
        if (!lblStatus.equals("You won this round"))
            checkMove(1);
        else j = 0;
    }//GEN-LAST:event_lblGreen_btnMousePressed

    private void lblGreen_btnMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblGreen_btnMouseReleased
        
         lblGreen_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Green_Light.jpg")));
        
    }//GEN-LAST:event_lblGreen_btnMouseReleased

//-------------------------------------לחצן אדום-----------------------------------------------    
    
    private void lblRed_btnMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblRed_btnMousePressed
        
        
        lblRed_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Red.jpg")));
        if (!lblStatus.equals("You won this round"))
            checkMove(2);
        else j = 0;
    }//GEN-LAST:event_lblRed_btnMousePressed

    private void lblRed_btnMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblRed_btnMouseReleased
        
         lblRed_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Red_Light.jpg")));
        
    }//GEN-LAST:event_lblRed_btnMouseReleased

//-------------------------------------לחצן כחול-----------------------------------------------
    
    private void lblBlue_btnMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBlue_btnMousePressed
        
        lblBlue_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Blue.jpg")));
        if (!lblStatus.equals("You won this round"))
            checkMove(4);
        else j = 0;
    }//GEN-LAST:event_lblBlue_btnMousePressed

    private void lblBlue_btnMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBlue_btnMouseReleased
        
         lblBlue_btn.setIcon(new ImageIcon(getClass().getResource("/Images/images_Blue_Light.jpg")));
        
    }//GEN-LAST:event_lblBlue_btnMouseReleased

    private void btnExitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExitMouseClicked
        int result = JOptionPane.showConfirmDialog(null, "Do you want to save the game?", "Please wait", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (result == JOptionPane.NO_OPTION)
            dispose();
        else if (result == JOptionPane.YES_OPTION){
            // save game to database
        }
    }//GEN-LAST:event_btnExitMouseClicked
    

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GameFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GameFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnDraft;
    public javax.swing.JButton btnExit;
    public javax.swing.JButton btnNextTurn;
    public javax.swing.JButton btnRestartGame;
    private javax.swing.JLabel jLabel6;
    public javax.swing.JPanel jPanel1;
    public javax.swing.JLabel lblBlue_btn;
    public javax.swing.JLabel lblGreen_btn;
    public javax.swing.JLabel lblLevel;
    public javax.swing.JLabel lblPlayerName;
    public javax.swing.JLabel lblRed_btn;
    public javax.swing.JLabel lblSaves;
    public javax.swing.JLabel lblStatus;
    public javax.swing.JLabel lblYellow_btn;
    // End of variables declaration//GEN-END:variables
}
