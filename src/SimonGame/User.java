package SimonGame;
/**
 * this class contains all the parameters of 
 * the User with getters and setters
*/

public class User {

    private int ID;
    private String nickName;
    private String email;
    private String password;
    private int status;
    
    //אתחול ריק
    public User(){    
    }
    
    //אתחול עם פרמטרים
    public User(int ID, String nickName, String email, String password, int status) {
        this.ID = ID;
        this.nickName = nickName;
        this.email = email;
        this.password = password;
        this.status = status;
    }
    
    //this is all the Setters and Getters
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    public String getNickName() {
        return nickName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getID() {
        return ID;
    }

    public int getStatus() {
        return status;
    }
    
    //הדפסה של כל הפרמטרים בשרשור לשורה אחת
    @Override
    public String toString(){ 
        return "nickName: " + this.nickName + ", email: " + this.email + ", password: " + this.password + ", status: " + this.status;
    }
    
}
