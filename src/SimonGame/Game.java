package SimonGame;

/**
 * this class contains all the parameters
 * of the game with Setters and Getters
 * also contains the logic functions
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Game {
    //הפרמטרים
    private int gameID;
    private int userID;
    private LinkedList<Integer> moves = new LinkedList<>();
    private int statusID;
    private int playerMove;

    //אתחול ריק
    public Game() {
    } 
     
    //אתחול עם פרמטרים
    public Game(int gameID, int userID, LinkedList<Integer> moves, int statusID) {
        this.gameID = gameID;
        this.userID = userID;
        this.moves.addAll(moves);
        this.statusID = statusID;
    }

    //this is all the Setters and Getters
    public void setPlayerMove(int playerMove) {
        this.playerMove = playerMove;
    }

    public int getPlayerMove() {
        return playerMove;
    }
    
    public void setGameID(int gameID) {
        this.gameID = gameID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public void setMoves(LinkedList<Integer> moves) {
        this.moves = moves;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }

    public int getGameID() {
        return gameID;
    }

    public int getUserID() {
        return userID;
    }

    public LinkedList<Integer> getMoves() {
        return moves;
    }

    public int getStatusID() {
        return statusID;
    }
    
    //printing the moves array to one string
    public String movesToString(){
        return Arrays.toString(moves.toArray());
    }
    
    /*
     פונקציה זו מבצעת עצירה בין כל צעד במערך
    כדי להציג את המערך בסדר אחד אחרי השני   
    ולא בבת אחת                             
     */ 
    public void waitBetweenMoves(){
        for (Integer move : moves) {
            System.out.println(move + " ");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.err.println("Error waitBetweenMoves");
            }
        }
    }
    
    //פונקציה שמדפיסה את כל הפרמטרים של המשחק בשרשור לשורה אחת
    @Override
    public String toString(){
        return "gameID: " + this.gameID + ", userID: " + this.userID + ", moves: " + this.movesToString() + ", statusID: " + this.statusID;
    }
    
    //פונקציה זו מוסיפה צעד בצורה רנדומלית למערך
    public void addMove(){
        Random rand = new Random();
        int newMove = 1 + rand.nextInt(4);
        this.moves.add(newMove);   
    }
    
    /*
        פונקציה זו בודקת אם הצעד שהשחקן עשה תואם
        את הצעד במערך ומחזירה אמת או שקר        
    */
    public boolean checkMove(int index) {
        System.out.println("the move: " + moves.get(index) + " , player move: " + this.playerMove);
        return (this.playerMove == moves.get(index));
    }
}
