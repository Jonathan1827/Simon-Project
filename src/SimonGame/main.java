package SimonGame;

import java.util.Scanner;

public class main {
    
    public static void main(String args[]) {
        
        Scanner input = new Scanner(System.in);
       
        
        Game game1 = new Game();
        game1.setGameID(1);
        game1.setUserID(23);
        game1.setStatusID(1);
        System.out.println("start game: \n" + game1.toString());
       
        while (game1.getStatusID() == 1) {
            game1.addMove();
            game1.waitBetweenMoves();
            for (int i = 0; i < game1.getMoves().size(); i++) {
                System.out.print("enter move: ");
                game1.setPlayerMove(input.nextInt());
                if (!game1.checkMove(i)) {
                    game1.setStatusID(2);
                    System.err.println("wrong move ");
                    break;
                }
            }
        }               
    }    
}
